#!/bin/sh

set -ex
# XXX: fixme: should derive from upgraded packages
DATE="$(date +%Y%m%d)"
MSG="cross/gcc-*: upgrade to ~$DATE"
BRANCH="monitoring-gcc-$DATE"

if [ $(git status --porcelain=1 | wc -l) -eq 0 ]; then
	echo "No changes to commit"
	exit 0
fi

# XXX: pass in needed vars rather than this awful hack
# sudo -E preserves environment
USER=pmos
HOME=/home/$USER

grev() {
	git rev-parse --verify $1
}

BASEDIR="$PWD"
cd /home/pmos/.local/var/pmbootstrap/cache_git/pmaports

git config --local user.name "pmOS CI"
git config --local user.email "pmos-ci@blah.com"

if [ "$(grev $BRANCH || true)" != "" ]; then
	echo "Branch $BRANCH already exists, ammending"
	git stash push . || true
	git checkout $BRANCH
	git stash pop || true
	git add -A
	git commit --amend --no-edit
	echo "Amended pmaports commit:"
else
	git status
	git add -A
	git commit -m "$MSG"
	echo "Created pmaports commit:"
fi

git show -1
git remote remove ssh_origin || true # local repo state may be cached
git remote add ssh_origin "git@$CI_SERVER_HOST:sdm845-ci/pmaports.git"
eval $(ssh-agent -s)
echo "$PMAPORTS_SSH_PUSH_KEY" | tr -d '\r' | ssh-add -
git checkout -b "$BRANCH"
GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no" git push --force ssh_origin HEAD:$BRANCH

PRIVATE_TOKEN="$PMAPORTS_CI_TOKEN"
# project ID of fork
PROJECT_ID="47954228"
. $BASEDIR/.ci/gitlab_api.sh

glapi_open_mr "$BRANCH" "$MSG" ""
